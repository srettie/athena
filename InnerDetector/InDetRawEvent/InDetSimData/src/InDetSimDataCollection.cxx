/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/


#include "InDetSimData/InDetSimDataCollection.h"

InDetSimDataCollection::InDetSimDataCollection() 
= default;

InDetSimDataCollection::~InDetSimDataCollection() 
= default;

